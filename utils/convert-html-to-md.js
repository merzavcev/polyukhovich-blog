const fs = require('fs');
const TurndownService = require('turndown')

const turndownService = new TurndownService()

const postsPath = '../content/posts/';
const htmls = fs.readdirSync(postsPath).filter( item => item.endsWith('.html') )

htmls.forEach(function(fileName) {
    const htmlContent = fs.readFileSync(`${postsPath}${fileName}`, 'utf8');
    const mdContent = turndownService.turndown(htmlContent);
    fs.writeFileSync(`${postsPath}${fileName.replace('.html', '.md')}`, mdContent);
});